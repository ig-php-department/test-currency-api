## Installation

You must have [docker compose](https://docs.docker.com/compose/install/) and [npm](https://www.npmjs.com/get-npm) installed.

Enter to the docker folder:
Copy file `.env.example` to `.env`

**Pay attention before run !!! Your ports 8000, 3306, 44300 should be free.** `lsof -i -P -n | grep LISTEN`

Build and run images: `docker-compose up --build -d`

Run rate's fetcher: `./symfony-console app:update-rates`
Or wait for a minute while cron event fire.

### FrontEnd Building

Enter to the app folder
Run command `npm install`

To build in developer mode: `npm run dev`.
To build in production mode: `npm run build`.

Go to http://localhost:8000

## Docker helpers 

Run symfony console: `./symfony-console`

Run composer: `./composer install`

## API

Go to http://localhost:8000/api and look at documentation

Get all pairs:

    curl -X GET "http://localhost:8000/api/pairs" -H "accept: application/json"
    
Get rates:

    curl -X GET "http://localhost:8000/api/rates?pair.id=1&page=1" -H "accept: application/json"
    
## Work flow

Command *app:update-rates* runs every minute by cron. It fetches rates from exmo, aggregate it by hour and store to database.
Go to http://localhost:8000/api 

