<?php


namespace App\Service\ExchangeRateService;


use App\Service\ExchangeRateService\Model\ExchangeRateInterface;
use App\Service\ExchangeRateService\Model\ExchangeRateReaderInterface;
use App\Service\ExchangeRateService\Model\ExchangeRateWriterInterface;
use Exception;

abstract class ExchangeRates implements ExchangeRatesInterface
{
    private string $rateEntityClass;

    abstract protected function fetchData(): array;

    abstract protected function convertData(array $data): ExchangeRateInterface;


    /**
     * @param string $rateEntityClass class witch implements interface ExchangeRateInterface
     * @throws Exception
     */
    public function __construct(string $rateEntityClass)
    {
        $this->rateEntityClass = $rateEntityClass;
        $this->validateRateEntityClass();
    }

    /**
     * @throws Exception
     */
    private function validateRateEntityClass()
    {
        $expects = [
            ExchangeRateReaderInterface::class,
            ExchangeRateWriterInterface::class,
        ];
        $implements = class_implements($this->rateEntityClass);
        $diff = array_diff($expects, $implements);
        if ($diff) {
            throw new Exception($this->rateEntityClass . ' does not implement interfaces ' . implode(', ', $diff));
        }
    }

    public function getRates(): array
    {
        $data = $this->fetchData();
        return array_map([$this, 'convertData'], $data);
    }

    protected function makeRateEntity(): ExchangeRateInterface
    {
        return new $this->rateEntityClass();
    }
}