<?php


namespace App\Service\ExchangeRateService;


use App\Service\ExchangeRateService\Model\ExchangeRateInterface;
use Symfony\Component\HttpClient\HttpClient;

class ExchangeRatesExmo extends ExchangeRates
{
    const TICKER = 'https://api.exmo.com/v1.1/ticker';

    protected function fetchData(): array
    {
        $tickerData = $this->fetch();
        $rates = json_decode($tickerData, true);
        $rates = $rates ?? [];
        $rates = array_map(
            fn($values, $pair) => $values + ['pair' => $pair],
            array_values($rates),
            array_keys($rates)
        );

        return $rates;
    }

    protected function convertData(array $data): ExchangeRateInterface
    {
        return $this->makeRateEntity()
            ->setPair($data['pair'])
            ->setHigh($data['high'])
            ->setLow($data['low'])
            ->setAvg($data['avg'])
            ->setUpdateTime($data['updated']);
    }


    private function fetch(): string
    {
        $client = HttpClient::create();
        $opts = [
            'max_duration' => 5.0,
        ];
        try {
            $response = $client->request('GET', self::TICKER, $opts);
            $body = $response->getContent();
        } catch (\Throwable $e) {
            $body = "";
        }

        return $body;
    }
}