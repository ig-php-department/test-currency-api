<?php

namespace App\Service\ExchangeRateService;

use App\Service\ExchangeRateService\Model\ExchangeRateReaderInterface;

interface ExchangeRatesInterface
{
    /**
     * @return ExchangeRateReaderInterface[]
     */
    public function getRates(): array;
}