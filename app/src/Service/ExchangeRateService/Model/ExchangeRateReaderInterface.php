<?php


namespace App\Service\ExchangeRateService\Model;


interface ExchangeRateReaderInterface
{
    public function getPair(): ?string;

    public function getHigh(): ?string;

    public function getLow(): ?string;

    public function getAvg(): ?string;

    public function getUpdateTime(): ?int;
}