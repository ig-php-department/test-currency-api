<?php


namespace App\Service\ExchangeRateService\Model;


interface ExchangeRateInterface extends ExchangeRateWriterInterface, ExchangeRateReaderInterface
{
}