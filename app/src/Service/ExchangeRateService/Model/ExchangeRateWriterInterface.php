<?php


namespace App\Service\ExchangeRateService\Model;


interface ExchangeRateWriterInterface
{
    public function setPair(string $pair): self;

    public function setHigh(string $high): self;

    public function setLow(string $low): self;

    public function setAvg(string $avg): self;

    public function setUpdateTime(int $avg): self;
}