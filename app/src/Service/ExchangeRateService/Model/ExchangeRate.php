<?php

namespace App\Service\ExchangeRateService\Model;


class ExchangeRate implements ExchangeRateInterface
{

    private ?string $pair;

    private ?string $high;

    private ?string $low;

    private ?string $avg;

    private ?int $updateTime;


    public function getPair(): ?string
    {
        return $this->pair;
    }

    public function setPair(string $pair): self
    {
        $this->pair = $pair;

        return $this;
    }

    public function getHigh(): ?string
    {
        return $this->high;
    }

    public function setHigh(string $high): self
    {
        $this->high = $high;

        return $this;
    }

    public function getLow(): ?string
    {
        return $this->low;
    }

    public function setLow(string $low): self
    {
        $this->low = $low;

        return $this;
    }

    public function getAvg(): ?string
    {
        return $this->avg;
    }

    public function setAvg(string $avg): self
    {
        $this->avg = $avg;

        return $this;
    }


    public function getUpdateTime(): ?int
    {
        return $this->updateTime;
    }


    public function setUpdateTime(int $updateTime): self
    {
        $this->updateTime = $updateTime;

        return $this;
    }
}
