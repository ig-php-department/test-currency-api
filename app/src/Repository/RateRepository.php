<?php

namespace App\Repository;

use App\Entity\Rate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    public function persistHourRate(Rate $rate): bool
    {
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare('INSERT INTO rate(pair_id, high, low, avg, hour)
            VALUES (:pair, :high, :low, :avg, :hour)
            ON DUPLICATE KEY UPDATE 
            high = IF(high>=VALUES(high), high, VALUES(high)),
            low = IF(low<=VALUES(low), low, VALUES(low)),
            avg = (avg+VALUES(avg))/2
        ');

        return $stmt->execute([
            'pair' => $rate->getPair()->getId(),
            'high' => $rate->getHigh(),
            'low' => $rate->getLow(),
            'avg' => $rate->getAvg(),
            'hour' => $rate->getHour(),
        ]);
    }

    // /**
    //  * @return Rate[] Returns an array of Rate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rate
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
