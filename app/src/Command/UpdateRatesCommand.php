<?php

namespace App\Command;

use App\Service\ExchangeRateService\Model\ExchangeRateReaderInterface;
use App\Entity\Rate;
use App\Service\ExchangeRateService\ExchangeRatesInterface;
use App\Repository\PairRepository;
use App\Repository\RateRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateRatesCommand extends Command
{
    protected static $defaultName = 'app:update-rates';
    /**
     * @var RateRepository
     */
    private RateRepository $rateRepository;
    /**
     * @var PairRepository
     */
    private PairRepository $pairRepository;

    /**
     * @var ExchangeRatesInterface
     */
    private ExchangeRatesInterface $exchangeRateRepository;

    public function __construct(
        RateRepository $rateRepository,
        PairRepository $pairRepository,
        ExchangeRatesInterface $exchangeRateRepository,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->rateRepository = $rateRepository;
        $this->pairRepository = $pairRepository;
        $this->exchangeRateRepository = $exchangeRateRepository;
    }

    protected function configure()
    {
        $this->setDescription('Fetch rates from ticker API');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $rates = $this->exchangeRateRepository->getRates();
        array_walk($rates, [$this, 'persistRate']);
        $io->success(sprintf('Rates parsed: %d', count($rates)));

        return 0;
    }


    private function persistRate(ExchangeRateReaderInterface $rate): bool
    {
        $pairName = $rate->getPair();
        $pair = $this->pairRepository->getByTitle($pairName) ?? $this->pairRepository->createNew($pairName);
        $rate = (new Rate())
            ->setPair($pair)
            ->setHigh($rate->getHigh())
            ->setLow($rate->getLow())
            ->setAvg($rate->getAvg())
            ->setHour($rate->getUpdateTime());
        return $this->rateRepository->persistHourRate($rate);
    }
}
