<?php

namespace App\Entity;

use App\Repository\RateRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/**
 * @ApiResource(
 *     attributes={"pagination_items_per_page"=72},
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass=RateRepository::class)
 */
class Rate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Pair::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiFilter(NumericFilter::class, properties={"pair.id"})
     */
    public $pair;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=8)
     */
    private $high;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=8)
     */
    private $low;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=8)
     */
    private $avg;

    /**
     * @ORM\Column(type="integer")
     * @ApiFilter(RangeFilter::class, properties={"hour"})
     */
    private $hour;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPair(): ?Pair
    {
        return $this->pair;
    }

    public function setPair(?Pair $pair): self
    {
        $this->pair = $pair;

        return $this;
    }

    public function getHigh(): ?string
    {
        return $this->high;
    }

    public function setHigh(string $high): self
    {
        $this->high = $high;

        return $this;
    }

    public function getLow(): ?string
    {
        return $this->low;
    }

    public function setLow(string $low): self
    {
        $this->low = $low;

        return $this;
    }

    public function getAvg(): ?string
    {
        return $this->avg;
    }

    public function setAvg(string $avg): self
    {
        $this->avg = $avg;

        return $this;
    }

    public function getHour(): ?int
    {
        return $this->hour;
    }

    public function setHour(int $timestamp): self
    {
        $this->hour = floor($timestamp / 3600) * 3600;

        return $this;
    }
}
