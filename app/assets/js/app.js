import Vue from 'vue';
import App from './components/App';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import moment from 'moment';
Vue.prototype.$moment = moment;

import '../css/app.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

new Vue({
    el: '#app',
    render: h => h(App)
});
